`include "pgm_accel_defines.v"

module pgm_accel_adc_de0(
	input iCLK,
	input iRESET,
	
	input 	iEN,
	output	oADC_CS,
	output	oADC_CLK,
	output	oADC_ADDR,
	input		iADC_DATA,
	
	output [11:0] oDATA,
	output oRDY
);

reg cs, scl;
reg scl_delay;

reg [5:0] cnt_scl;
reg [4:0] cnt_abs;

reg [11:0] out_data;

wire SW_SCL = (cnt_scl == `SCL_ADC);
wire RDY = (cnt_abs == 5'd16 && !cs);
wire GET_DATA = (cnt_abs >= 5'd4 && cnt_abs <= 5'd16);
wire FRONT_SCL = (!scl_delay & scl);

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cs <= 1'b1;
	else if(iEN) cs <= 1'b0;
	else if(RDY) cs <= 1'b1;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnt_abs <= 5'd0;
	else if(!cs && FRONT_SCL) cnt_abs <= cnt_abs + 1'b1;
	else if(cs) cnt_abs <= 5'd0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnt_scl <= `SCL_ADC;
	else if(!cs) 
		begin
			if(SW_SCL) cnt_scl <= 6'd0;
			else cnt_scl <= cnt_scl + 1'b1;
		end
	else cnt_scl <= `SCL_ADC;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) scl_delay <= 1'b0;
	else scl_delay <= scl;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) scl <= 1'b1;
	else if(!cs && SW_SCL) scl <= ~scl;
	else if(cs) scl <= 1'b1;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) out_data <= 12'd0;
	else if(!cs && FRONT_SCL && GET_DATA) out_data <= {out_data[10:0], iADC_DATA}; 
	else if(cs) out_data <= 12'd0;
end

assign oADC_CS = cs;
assign oADC_CLK = scl;
assign oADC_ADDR = 1'b0;

assign oDATA = out_data;
assign oRDY = RDY;

endmodule 