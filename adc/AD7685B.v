`define SCL_ADC 8'd12
`define T_CONV 8'd110 // 2.2us
`define T_EN 8'd1 // 20ns

module AD7685B(
	input iCLK,
	input iRESET,
	
	input iEN,

// ADC:
	input	iADC_DATA,
	output	oADC_CNV,
	output	oADC_SCL,

// CONTROL:
	output	[15 : 0] oDATA,
	output	oRDY
);

reg cnv;
reg rdy;

reg scl;
reg scl_shift;

reg [15 : 0] data;
reg [15 : 0] req_data;

reg [7 : 0] timer;
reg [4 : 0] bit_cnt;

reg [2 : 0] state;

localparam stage_0 = 3'd0;
localparam stage_1 = 3'd1;
localparam stage_2 = 3'd2;
localparam stage_3 = 3'd3;
localparam stage_4 = 3'd4;
localparam stage_5 = 3'd5;
localparam stage_6 = 3'd6;
localparam stage_7 = 3'd7;

wire ISSTAGE0 = state == stage_0;
wire ISSTAGE1 = state == stage_1;
wire ISSTAGE2 = state == stage_2;
wire ISSTAGE3 = state == stage_3;
wire ISSTAGE4 = state == stage_4;
wire ISSTAGE5 = state == stage_5;
wire ISSTAGE6 = state == stage_6;
wire ISSTAGE7 = state == stage_7;

wire GETEN = (ISSTAGE0 & iEN);

wire CNVCOMPLETE =	(ISSTAGE1 & (timer == `T_CONV));
wire SDORDY =		(ISSTAGE2 & (timer == `T_EN));
wire SW_SCL =		(ISSTAGE3 & (timer == `SCL_ADC));

wire DATARDY = (bit_cnt == 5'd16);
wire FRONT_SCL = (scl_shift & !scl);

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) state <= 3'd0;
	else 
		case(state)
			stage_0: if(iEN) state <= stage_1;			// waiting iEN
			stage_1: if(CNVCOMPLETE) state <= stage_2;	// waiting conv
			stage_2: if(SDORDY) state <= stage_3;		// waiting valid SDO
			stage_3: if(DATARDY) state <= stage_4;		// waiting last bit
			stage_4: state <= stage_0; // ready
			default: state <= state;
		endcase
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) bit_cnt <= 5'd0;
	else if(ISSTAGE3) 
		begin
			if(FRONT_SCL) bit_cnt <= bit_cnt + 5'd1;
			else bit_cnt <= bit_cnt;
		end
	else bit_cnt <= 5'd0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) req_data <= 16'd0;
	else if(FRONT_SCL) req_data <= {req_data[15 : 0], iADC_DATA};
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) data <= 16'd0;
	else if(ISSTAGE4) data <= req_data;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) timer <= 8'd0;
	else 
		begin
			if(ISSTAGE0 | CNVCOMPLETE | SDORDY | SW_SCL) timer <= 0;
			else if (ISSTAGE1 | ISSTAGE2 | ISSTAGE3) timer <= timer + 1'b1;
			else timer <= timer;
		end
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) begin
		scl <= 1'b0;
		scl_shift <= 1'b0;
	end
	else 
		begin
			scl_shift <= scl;
			
			if(ISSTAGE3) 
				begin
					if(SW_SCL) scl <= ~scl;
					else scl <= scl;
				end
			else scl <= 1'b0;
		end
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnv <= 1'b0;
	else if(ISSTAGE1) cnv <= 1'b1;
	else cnv <= 1'b0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) rdy <= 1'b0;
	else if(ISSTAGE4) rdy <= 1'b1;
	else rdy <= 1'b0;
end

assign oADC_CNV = cnv;
assign oADC_SCL = scl;
assign oDATA = data;
assign oRDY = rdy;

endmodule 