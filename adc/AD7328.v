`define SCL_ADC 6'd50

module AD7328(
	input iCLK,
	input iRESET,
	
	input 	iEN,
	
	input		iADC_DATA,
	output	oADC_DATA,
	output	oADC_CS,
	output	oADC_CLK,
	
	output [11 : 0] oDATA,
	output oRDY
);

reg [2 : 0] state;

reg [14 : 0] cnt_idle;

reg cs, scl;
reg scl_delay;

reg [5 : 0] cnt_sw_scl;
reg [4 : 0] cnt_abs;

reg [14 : 0] resp_data;
reg [14 : 0] req_data;

localparam S_IDLE = 0;
localparam S_CONTROL = 1;	// write control reg in ADC for config mode
localparam S_RANGE = 2;		// write range reg in ADC for config mode
localparam S_EXP_EN = 3;	// expect enable from main module
localparam S_SEND = 4;		// recieve data from ADC
localparam S_RDY = 5;

wire IDLE = (state == S_IDLE);
wire CONTROL = (state == S_CONTROL);
wire RANGE = 	(state == S_RANGE);

wire IDLE_CNT = (cnt_idle == 15'd30_000); // requred wait ~500 us after turn on ADC

wire SW_SCL	   = 	(cnt_sw_scl == `SCL_ADC); // SCL freq
wire END_PACKET = (cnt_abs == 5'd16);
wire RDY 	   = 	(END_PACKET && cs);
wire GET_DATA  =	(cnt_abs >= 5'd3 && cnt_abs <= 5'd14); // mb 4 - 15
wire FRONT_SCL =	(!scl_delay & scl);

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) state <= 3'd0;
	else 
		case(state)
			S_IDLE:		if(IDLE_CNT) state <= S_CONTROL;
			S_CONTROL:	if(RDY) state <= S_RANGE;  // main module may ignore out data from this state
			S_RANGE:		if(RDY) state <= S_EXP_EN; // main module may ignore out data from this state
			S_EXP_EN:	if(iEN) state <= S_SEND;
			S_SEND:		if(RDY) state <= S_RDY;
			S_RDY:		state <= S_EXP_EN;
		endcase
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnt_idle <= 15'd0;
	else if(IDLE) cnt_idle <= cnt_idle + 1'b1;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cs <= 1'b1;
	else if(iEN | ((CONTROL | RANGE) & cnt_abs == 5'd0)) cs <= 1'b0;
	else if((END_PACKET & cnt_sw_scl == 6'd49) | IDLE) cs <= 1'b1;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnt_abs <= 5'd0;
	else if(!cs && FRONT_SCL) cnt_abs <= cnt_abs + 1'b1;
	else if(cs) cnt_abs <= 5'd0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) cnt_sw_scl <= `SCL_ADC;
	else if(!cs) 
		begin
			if(SW_SCL) cnt_sw_scl <= 6'd0;
			else cnt_sw_scl <= cnt_sw_scl + 1'b1;
		end
	else cnt_sw_scl <= 6'd0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) scl_delay <= 1'b0;
	else scl_delay <= scl;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) scl <= 1'b1;
	else if(!cs && SW_SCL) scl <= ~scl;
	else if(cs) scl <= 1'b1;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) req_data <= 15'd0;
	else if(!cs & FRONT_SCL & GET_DATA) req_data <= {req_data[13 : 0], iADC_DATA}; 
	else if(cs) req_data <= 15'd0;
end

always@(posedge iCLK or negedge iRESET)begin
	if(!iRESET) resp_data <= 15'd0;
	else if(cs)
		begin
			if(CONTROL) resp_data <= 15'b100_000_00_00_1_1_00_0; // control reg ADC (5 bit "coding" = 1, 4 bit "ref" = 1 from datasheet)
			else if (RANGE) resp_data <= 15'b101_10_0000000000; // range reg ADC (12, 11 bits "range" = 2'b10 "+/- 2.5 V" from datasheet)
			else resp_data <= 15'd0;
		end 
	else if(FRONT_SCL) resp_data <= {resp_data[13 : 0], 1'b0}; 
end

assign oADC_DATA =	resp_data[14];
assign oADC_CS   = 	cs;
assign oADC_CLK  = 	scl;

assign oDATA  =  	req_data[11 : 0];
assign oRDY   = 	RDY;

endmodule 