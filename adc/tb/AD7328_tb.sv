`timescale 1ns/1ns

`define TACT 20
`define HALF_TACT `TACT/2

module AD7328_tb;

bit clk;
bit reset;

bit adc_en;
bit adc_ser_data;
bit [11:0] adc_par_data;

wire [11:0] ADC_OUT_DATA;
wire ADC_CS, ADC_SCL;
wire ADC_RDY;

initial begin
	$timeformat(-9,0," ns",1);
	clk = 0;
	forever	#(`HALF_TACT) clk = ~ clk;
end

initial begin
	reset = 1'b0; #(`TACT);
	reset = 1'b1; #(2*`TACT);
end

initial begin
	integer i = 0;
	adc_en = 1'b0;
	wait(DUT.state == 3) // state "expect enable"
	#(10*`TACT);
	
	$display("\n\n		START SEND FROM DUT\n");
	repeat(10)
		begin
			adc_par_data =  $unsigned($random)%4095;
			if(i == 9) adc_par_data = 12'hFFF;
			
			#1;
			adc_en = 1'b1;
			#(`TACT);
			adc_en = 1'b0;
			
			wait(ADC_RDY);
			assert(adc_par_data == ADC_OUT_DATA) $display("OK: 0b%b, 0d%d, 0x%h, %t", ADC_OUT_DATA, ADC_OUT_DATA, ADC_OUT_DATA, $time);
			else $display("ER:	rec   = 0b%b, 0d%d, 0x%h,\n   	trans = 0b%b, 0d%d, 0x%h, %t", adc_par_data, adc_par_data, adc_par_data, ADC_OUT_DATA, ADC_OUT_DATA, ADC_OUT_DATA, $time);
			#(10*`TACT);
			
			i = i + 1;
		end
	
	$display("\n		COMPLETE\n");
	mti_fli::mti_Cmd("stop -sync");
end

initial begin
	adc_ser_data = 1'bz;
	#(4*`TACT);
	
	wait(DUT.cnt_abs == 3);
	adc_ser_data = 1'b0;
	
	repeat(100)
		begin
			repeat(12)
				begin
					if(DUT.cnt_abs <= 15)
						begin
							wait(!ADC_SCL);
							adc_ser_data = adc_par_data[14 - DUT.cnt_abs];
							wait(ADC_SCL);
						end
					else break;
				end
		end
	
	wait(ADC_CS);
	#(4*`TACT);
end

AD7328 DUT(
	.iCLK(clk),
	.iRESET(reset),
	
	.iEN(adc_en),
	
	.iADC_DATA(adc_ser_data),
	.oADC_DATA(),
	.oADC_CS(ADC_CS),
	.oADC_CLK(ADC_SCL),
	
	.oDATA(ADC_OUT_DATA),
	.oRDY(ADC_RDY)
);

endmodule