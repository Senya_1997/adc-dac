`timescale 1ns/1ns

`define TACT 20
`define HALF_TACT `TACT/2

module adc_de0_tb; //ADC 12 bit on DE0_Nano

bit clk;
bit reset;

bit adc_en;
bit adc_ser_data;
bit [11:0] adc_par_data;

wire [11:0] ADC_OUT_DATA;
wire ADC_CS, ADC_SCL;
wire ADC_RDY;

initial begin
	$timeformat(-9,0," ns",1);
	clk = 0;
	forever	#(`HALF_TACT) clk = ~ clk;
end

initial begin
	reset = 1'b0; #(`TACT);
	reset = 1'b1; #(2*`TACT);
end

initial begin
	adc_en = 1'b0;
	#(10*`TACT);
	
	repeat(10)
		begin
		
			#1;
			adc_en = 1'b1;
			#(`TACT);
			adc_en = 1'b0;
			wait(ADC_RDY);
			assert(adc_par_data == ADC_OUT_DATA) $display("OK: 0b%b, %t", ADC_OUT_DATA, $time);
			else $display("ER: rec = 0b%b, trans = 0b%b, %t", adc_par_data, ADC_OUT_DATA, $time);
			#(10*`TACT);
		end
end

initial begin
	adc_ser_data = 1'bz;
	#(4*`TACT);
	
	repeat(100)
		begin
			adc_par_data =  $unsigned($random)%4000;
			
			wait(DUT.ADC.cnt_abs == 4);
			adc_ser_data = 1'b0;
			
			repeat(12)
				begin
					if(DUT.ADC.cnt_abs <= 15)
						begin
							wait(!ADC_SCL);
							adc_ser_data = adc_par_data[15 - DUT.ADC.cnt_abs];
							wait(ADC_SCL);
						end
					else break;
				end
			
			wait(ADC_CS);
			#(4*`TACT);
		end
end

adc_de0 DUT(
	.iCLK(clk),
	.iRESET(reset),
	
	.iEN(adc_en),
	.oADC_CS(ADC_CS),
	.oADC_CLK(ADC_SCL),
	.oADC_ADDR(),
	.iADC_DATA(adc_ser_data),
	
	.oDATA(ADC_OUT_DATA),
	.oRDY(ADC_RDY)
);

endmodule