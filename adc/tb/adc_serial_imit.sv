`include "./imit.svh"

module adc_serial_imit #(

// config:
	parameter D_BIT = 16,

	parameter MIN_T_CONV	= 500ns,
	parameter MAX_T_EN		= 15ns,

	parameter SPI_DIRECT	= `SPI_MSB,
	parameter SPI_ISSUE		= `SPI_NEGEDGE,

	parameter TEST = `TEST_CNT,
	parameter DIR_SAMPLE = ""
)(
	input iCNV,
	input iSCL,
	
	output oDATA
);

typedef logic signed [D_BIT - 1 : 0] adc_data_t;

adc_data_t adc_shift_reg;
adc_data_t adc_valid_data;

logic adc_data;

bit flag_adc_send = 0; // '1' - means that conversion was complete and CONV signal was correct

// test data:
	adc_data_t data_cnt = 0;
	integer f_sample = 0;

// get ADC data, start conv:
	always@(posedge iCNV)begin
		flag_adc_send = 1'b0;

		if(TEST == `TEST_RAND)
			adc_valid_data = adc_data_t'($random());
		else if(TEST == `TEST_CNT)
			begin
				adc_valid_data = data_cnt;
				data_cnt = data_cnt + 1'b1;
			end
		else
			begin
				if((f_sample == 0) || $feof(f_sample))
					begin
						$fclose(f_sample);
						
						f_sample = $fopen(DIR_SAMPLE, "r");
		
						if(f_sample == 0)
							begin
								$fclose(f_sample);
								$display("\n ***\tError: file name is wrong: '%s'\n", DIR_SAMPLE);
								$stop;
							end
					end
				
				void'($fscanf(f_sample, "%d\n", adc_valid_data));
			end
		
		adc_shift_reg = adc_valid_data;
		
		#(MIN_T_CONV);
		
		if(~iCNV)
			begin
				$display("\n ***\tError: ADC conv signal low on the end of conversion\n");
				$stop;
			end
		else
			begin
				wait(~iCNV);
				#(MAX_T_EN);
				flag_adc_send = 1'b1;
			end
	end

// issue ADC data:
	generate
		if(SPI_ISSUE == `SPI_NEGEDGE)
			always@(negedge iSCL)
				SendSerialData;
		else
			always@(posedge iSCL)
				SendSerialData;
	endgenerate

	task SendSerialData;
		if(flag_adc_send)
			begin
				if(SPI_DIRECT == `SPI_MSB)
					begin
						adc_data		=  adc_shift_reg[D_BIT - 1];
						adc_shift_reg	= {adc_shift_reg[D_BIT - 2 : 0], 1'bX};
					end
				else
					begin
						adc_data		= adc_shift_reg[0];
						adc_shift_reg	= {1'bX, adc_shift_reg[D_BIT - 1 : 1]};
					end
			end
	endtask

assign oDATA = adc_data;

endmodule 