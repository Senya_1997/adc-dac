`timescale 1ns/1ns

`define SPI_MSB	1
`define SPI_LSB	0

`define SPI_NEGEDGE	1
`define SPI_POSEDGE	0

`define TEST_SMPL	0
`define TEST_CNT	1
`define TEST_RAND	2
