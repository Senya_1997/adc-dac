`timescale 1ns/1ns

`define TACT 20
`define HALF_TACT `TACT/2

module adc_ad7685b_tb;

bit clk;
bit reset;

bit en;

initial begin
	$timeformat(-9,0," ns",1);
	clk = 1;
	forever	#(`HALF_TACT) clk = ~clk;
end

initial begin
	reset = 1'b1; #(2*`TACT);
	reset = 1'b0; #(`TACT);
	reset = 1'b1;
end

initial begin
	#(`TACT); en = 1'b0;
	#(5*`TACT); en = 1'b0;
	#(`TACT); en = 1'b1;
	#(`TACT); en = 1'b1;
	#(`TACT); en = 1'b0;
end

AD7685B DUT(
	.iCLK(clk),
	.iRESET(reset),
	
	.iEN(en),

// ADC:
	.iADC_DATA(en)
	.oADC_CNV(),
	.oADC_SCL(),

// CONTROL:
	.oDATA(),
	.oRDY()
);

endmodule