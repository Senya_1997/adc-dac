/*
AD9203
	max 40 MHz <=> 25 ns tact
	data delay 5.5 tact in ADC clk

latch ADC data on front:
	tsu(max)	= 25 - 7 = 18 ns
	th(max)		= 3 ns

latch ADC data on fall:
	tsu(max)	= 25/2 - 7 = 5.5 ns
	th(max)		= 25/2 + 3 = 15.5 ns
*/

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity AD9203 is generic (
	CH_NUM		: integer := 2; -- num of used ADC
	D_BIT		: integer := 10; -- ADC data width 
	DATA_DELAY	: integer := 8 -- tact in ADC clk
);
port (
	iCLK	: in std_logic;
	iRESET	: in std_logic;

	iSTART	: in std_logic;
	iSTOP	: in std_logic;

	iDFS	: in std_logic;

-- ADC inteface:
	iOTR	: in std_logic; -- out of range indicate
	iDATA	: in std_logic_vector (D_BIT - 1 downto 0);

	oCLK	: out std_logic;
	oDFS	: out std_logic; -- data format: 1 - twos complementary; 0 - straight binary
	oTRI_ST : out std_logic; -- 1 - HiZ; 0 - active out
	oSTBY 	: out std_logic; -- 1 - power down mode; 0 - normal

-- in FPGA:
	oDATA 	: out std_logic_vector (D_BIT - 1 downto 0);
	oVALID	: out std_logic;
	oOTR	: out std_logic
);

end entity AD9203;

architecture AD9203_a of AD9203 is
	constant START_DELAY : integer := 3; -- in tact of ADC clk

	signal start 	: std_logic;
	signal start_d 	: std_logic_vector (START_DELAY - 1 downto 0);

	signal cnt_delay_data : std_logic_vector (2 downto 0);

-- for ADC:
	signal adc_in_tri_st 	: std_logic;
	signal adc_in_stby 		: std_logic;

-- for FPGA:
	signal data 	: std_logic_vector (D_BIT - 1 downto 0);
	signal valid 	: std_logic;
	signal otr 		: std_logic;

	signal ADC_DATA_RDY : std_logic;
begin

ADC_DATA_RDY <= (cnt_delay_data >= DATA_DELAY);

process(iCLK, iRESET) begin
	if(rising_edge(iCLK)) then
		if(iRESET) then
			start <= '0';
		elsif(iSTART) then
			start <= '1';
		elsif(iSTOP) then
			start <= '0';
		end if;
	end if;
end process;

process(iCLK, iRESET) begin
	if(rising_edge(iCLK)) then
		if(iRESET) then
			cnt_delay_data <= '0';
		elsif(start)
			if(cnt_delay_data < DATA_DELAY) then
				cnt_delay_data <= cnt_delay_data + '1';
			end if;	
		else
			cnt_delay_data <= '0';
		end if;
	end if;
end process;

process(iCLK) begin
	if(rising_edge(iCLK)) then
		if(ADC_DATA_RDY) then
			data <= iDATA;
			otr <= iOTR;
		end if;
	end if;
end process;

process(iCLK) begin
	if(rising_edge(iCLK)) then
		start_d <= (start_d(START_DELAY - 2 downto 0), start);

		adc_in_tri_st <= (not start_d(START_DELAY - 1)) or (not start);
		adc_in_stby <= (not start_d(START_DELAY - 1)) and (not start);

		valid <= ADC_DATA_RDY;
	end if;
end process;

-- output
	-- ADC:
		oDFS <= iDFS;
		oCLK <= iCLK;

		oTRI_ST <= adc_in_tri_st;
		oSTBY <= adc_in_stby;

	-- FPGA:
		oDATA <= data;
		oOTR <= otr;
		oVALID <= valid;

end architecture AD9203_a;