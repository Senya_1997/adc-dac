`include "imit.svh"

module dac_serial_imit #(

// config:
	parameter D_BIT			= 16,
	parameter SPI_D_BIT		= 24,
	
	parameter COM_D_BIT		= 4,
	parameter COM_WR		= 4'b0011,

	parameter SPI_DIRECT	= `SPI_MSB,
	parameter SPI_LATCH		= `SPI_NEGEDGE,
	parameter SPI_CS_ACT	= `SPI_CS_LOW
)(
	input iRESET, // active low
	
	input iSCL,
	input iCS,
	input iDATA,
	
	output signed [D_BIT - 1 : 0] oDATA
);

logic signed [SPI_D_BIT - 1 : 0] dac_shift_reg = {(SPI_D_BIT){1'b0}};
logic signed [D_BIT - 1 : 0] dac_valid_data;

bit en_rec_data;

generate
// responce DAC data for check:
	if(SPI_CS_ACT == `SPI_CS_LOW)
		begin
			always@(negedge iCS)
				en_rec_data = 1'b1;
		
			always@(posedge iCS)
				GetLatchData;	
		end
	else
		begin
			always@(posedge iCS)
				en_rec_data = 1'b1;
				
			always@(negedge iCS)
				GetLatchData;
		end

// serial data:			
	if(SPI_LATCH == `SPI_NEGEDGE)
		always@(negedge iSCL)
			GetSerialData;
	else
		always@(posedge iSCL)
			GetSerialData;
endgenerate

task GetLatchData;
	begin
		en_rec_data = 1'b0;
		
		if(dac_shift_reg[SPI_D_BIT - 1 : SPI_D_BIT - COM_D_BIT] == COM_WR) // e.g. [23 : 20]
		
			dac_valid_data = dac_shift_reg[SPI_D_BIT - 1 - COM_D_BIT : SPI_D_BIT - COM_D_BIT - D_BIT]; // e.g. [19 : 4]
			
		else if(iRESET)
			begin
				$display("\n ***\tError: DAC control bits is not write command\n");
				$stop;
			end
	end
endtask

task GetSerialData;
	if(en_rec_data)
		begin
			if(SPI_DIRECT == `SPI_MSB)
				dac_shift_reg = {dac_shift_reg[SPI_D_BIT - 2 : 0], iDATA};
			else
				dac_shift_reg = {iDATA, dac_shift_reg[SPI_D_BIT - 1 : 1]};
		end
endtask

assign oDATA = dac_valid_data;
	
endmodule